\chapter{6LoWPAN-ND dans Contiki}
Dans ce chapitre, nous allons détailler certains points essentiels pour l'intégration de \acrshort{6LoWPAN-ND} au sein d'un petit système d'exploitation embarqué pour senseurs: Contiki. Nous allons aborder les choix pris pour permettre l'intégration de ce nouveau protocole. Certaines contraintes imposées par l'OS impliquent une adaptation de la spécification du \acrshort{6LoWPAN-ND} (RFC 6775 \cite{rfc6775}). Nous allons aussi exposer les choix faits par rapport à des points flous du RFC et aborder les améliorations. Le reste de l’implémentation de \acrshort{6LoWPAN-ND} est conforme aux directives strictes du RFC.




\section{Contiki}
Contiki \cite{contiki} est un système d'exploitation embarqué pour \acrshort{WSN}. Il est développé par le centre de recherche scientifique suédois: SICS. Ce système optimise sa mémoire tout en fournissant une connectivité avec Internet du point de vue IPv4 et IPv6. Il possède un certain nombre de librairies comme un Shell. Un simulateur, Cooja \cite{contiki}, permet d'émuler des \acrshort{WSN} et d'observer le comportement de Contiki. Nous l'utiliserons pour notre implémentation et l'analyse.
Pour réduire son empreinte mémoire et sa consommation électrique, Contiki utilise une micro stack IP et un mécanisme de processus Protothreads.

\subsection{Protothreads}
Protothreads est un compromis entre les avantages de la programmation event-driven et la programmation multi-threads. Le mécanisme a été développé pour un système avec des contraintes mémoires. L'écriture de code (figure \ref{fig:protothread_code}) est séquentielle avec la possibilité de blocage dans une approche événementielle. La différence entre ce modèle et les deux autres paradigmes de programmation ne réside pas seulement dans la structure du code, mais aussi sur sa longueur. Cette dernière caractéristique est directement liée à la mémoire ROM. Bien que les Protothreads possèdent les avantages des deux partis, ils ne fournissent pas de mécanisme de préemption ou d'historique des appels et des variables.

\begin{figure}[!h]
    \centering
    \begin{tabular}{|c|c|c|}
\hline 
\begin{lstlisting}
void
radio_thread(void)
{ 
  while(1) {
    radio_on();
    timer_set(&t, T_AWAKE);
    wait_timer(&t);
    radio_off();
    timer_set(&t, T_SLEEP);
    wait_timer(&t);
  } 
} 
\end{lstlisting} & 
\begin{lstlisting}
enum { 
  ON,
  OFF
} state;

void
timer_eventhandler()
{ 
  switch(state) {
  case OFF:
    if(timer_expired(&t)) {
      radio_on();
      state = ON;
      timer_set(&t, T_AWAKE);
    }
    break; 
  case ON:
    if(timer_expired(&t)) {
      radio_off();
      state = OFF;
    } 
    timer_set(&t, T_SLEEP);
    break;
  } 
}
\end{lstlisting} & 
\begin{lstlisting}
int
radio_protothread(struct pt *pt)
{
  PT_BEGIN(pt);
  while(1) {
    radio_on();
    timer_set(&t, T_AWAKE);
    PT_WAIT_UNTIL(pt, timer_expired(&t));
    radio_off();
    timer_set(&t, T_SLEEP);
    PT_WAIT_UNTIL(pt,
    timer_expired(&t));
  }
  PT_END(pt);
}
\end{lstlisting} \\ 
\hline 
Multi-thread & Event-driven & Protothread \\ 
\hline 
\end{tabular} 
    \caption{Exemples de programmation multi-thread, event-driven et protothread \cite{Vasseur:2010:ISO:1875341}}
    \label{fig:protothread_code}
\end{figure}



\subsection{uIP}
Contiki utilise une stack uIP fournissant une pleine connectivité IP tout en consommant peu de mémoire. La stack prend en charge IPv4 ou IPv6 avec ICMP, UDP et TCP. Il est possible de faire tourner une stack uIP avec 100 bytes de mémoire RAM \cite{Vasseur:2010:ISO:1875341} alors qu'une stack Unix en utilisera 1MB. %ref 

\begin{figure}[!h]
    \centering
    \includegraphics[width=10cm]{images/implem/uip.png}
    \caption{Operation pour la stack uIP}
    \label{fig:uip}
\end{figure}

Le principe des opérations dans la stack uIP est le suivant (figure \ref{fig:uip}). Les paquets entrants et venant du pilote de périphérique de communication passent par la stack uIP qui traite les données. Le traitement fini, il les donne à l’application correspondante. L'envoi de données effectue le chemin inverse et uIP ajoute des headers du protocole avant de les transmettre au périphérique de communication.
A l'intérieur de cette stack, il existe un processus de vérification des paquets. Il consiste à vérifier la version de l'IP, ensuite sa longueur et son adresse. Si l'adresse n'est pas la sienne, alors elle est forwardée, au contraire d'autres vérifications sont effectuées sur la couche supérieure: ICMP/UDP/TCP.

Nous nous occuperons de la partie ICMP, car le protocole \acrlong{ND} et \acrshort{RPL} sont déjà implémentés au sein Contiki.  L'ensemble de notre implémentation se base sur ce travail.

\subsection{Macro}
Contiki utilise un grand nombre de macros pour réduire son code compilé. Ceux-ci peuvent activer ou désactiver certains protocoles ou options pour économiser de la mémoire ROM. Ils permettent aussi l'ajout d'application dans le système d'exploitation.

Contiki ne prend pas en charge l'entité d'un  appareil. Or, \acrshort{6LoWPAN-ND} en utilise 3 différentes qui doivent être intégrées dans un seul code source. Pour ce faire, nous avons défini un certain nombre de macros pour la précompilation. Pour chaque entité, \acrshort{6LH}, \acrshort{6LR} et \acrshort{6LBR}, nous avons respectivement les macros: \emph{UIP\_CONF\_6LN}, \emph{UIP\_CONF\_6LR} et \emph{UIP\_CONF\_6LBR}. De plus, nous distinguons les types routeur \acrshort{6LoWPAN}, c'est-à-dire les \acrshort{6R}: \emph{UIP\_CONF\_6L\_ROUTER}. L'intégration de \acrshort{6LoWPAN-ND} ne doit en rien changer le code initial. En effet, la macro \emph{CONF\_6LOWPAN\_ND} est dédiée à l'activation de ce protocole.  L'utilisateur n'a besoin d'effectuer qu'une configuration minimale: il ajoute la macro de l'entité désirée dans le Makefile de son programme.




\section{Adressage IPv6}
Notre travail est centré sur l'IPv6, nous aborderons uniquement les structures et problèmes rencontrés sur cette version dans Contiki.

\subsection{Structure}
La figure \ref{fig:struct_netif} représente la structure interne de l'interface réseau dans Contiki. La structure \emph{uip\_ds6\_netif} n'est allouée qu'une seule fois, car nous n’avons qu'une interface réseau 802.4.15. IPv6 permet d'avoir un certain nombre d'adresse de différent type sur une même interface. Les adresses unicast utilisent la structure \emph{uip\_ds6\_addr}, anycast la structure \emph{uip\_ds6\_aadr} et multicast la structure \emph{uip\_ds6\_maddr}. Chaque adresse est ajoutée dans la liste du type correspondant. Nous pouvons avoir plusieurs adresses qui sont représentées par une étoile sur la figure \ref{fig:struct_netif}. 
Sur fond blanc, nous avons toutes les variables utilisées par défaut par Contiki. D'autres fonds indiquent que ceux-ci seront activés par la macro en légende.


\begin{figure}[!h]
    \centering
    \includegraphics[width=14cm]{images/implem/struct_netif.png}
    \caption{Structure interne de l'interface réseau}
    \label{fig:struct_netif}
\end{figure}

\subsection{Intégration}
Nous avons évoqué les différentes longueurs d'adresse data link dans la section \ref{sec:eui_16_64}. Au sein de Contiki, l'adressage IPv6 se fait par autoconfiguration et donc sur base de l'adresse MAC, section \ref{sec:build_iid}. A l'heure où nous écrivons ce travail, l'implémentation de la construction d'adresses se basant sur \acrshort{EUI-16} n'est que partiellement codée dans Contiki et inutilisable. En effet, certaines fonctions sont présentes, notamment dans la compression d'adresses sur 16 bits (\emph{sicslowpan.c}). Mais l'OS ne prend en compte que les adresses data link de 48 ou 64 bits. 6LoWPAN-ND propose un mécanisme de vérification de duplication d'adresse (avec les messages DAR e DAC) pour GP16. Nous l'avons appliqué aux adresses basées sur \acrshort{EUI-64} même si celles-ci devraient être uniques; tout en gardant à l'esprit une future prise en charge 16 bits. Si ceci est implémenté, il suffit de regarder, à la réception d'un \acrshort{NS}, si l'adresse est GP16 ou non. Si elle l'est, plutôt que d'envoyer un DAR au \acrshort{6LBR}, nous lui renvoyons un \acrshort{NA}.





\section{Neighbor Cache Entry}
\subsection{Structure}
Durant intégration de \acrshort{6LoWPAN-ND} dans Contiki, nous avons du manipuler les structures de la \acrshort{NC} et de la \acrshort{RT}, celles-ci n’ont nullement changées avec notre implémentation.

La figure \ref{fig:struct_nc} est construite à l'aide de deux listes. La première une table de structure \emph{nbr\_table\_key} qui possède les adresses link layer. La deuxième est l'ensemble des champs utilisé dans la \acrlong{NC}: \emph{uip\_ds6\_nbr}. L’association se fait par leurs index similaires entre les tables. Le champ \emph{next} est utilisé pour savoir si l'entrée est employée.
\begin{figure}[!h]
    \centering
    \includegraphics[width=14cm]{images/implem/struct_nc.png}
    \caption{Structure interne du Neighbor Cache}
    \label{fig:struct_nc}
\end{figure}

Les entrées dans la table de routage sont représentées par la structure \emph{uip\_ds6\_route} (figure \ref{fig:struct_rt}). Le nexthop est obtenu par le champ \emph{neighbor\_routes}. Le mécanisme pour obtenir l'adresse IP est une série de pointeurs et index dans des tables pour finalement obtenir l'entrée dans la table \emph{uip\_ds6\_nbr}. Le nexthop doit être dans la \acrlong{NC} pour autoriser l’ajout de l'entrée dans la \acrshort{RT}. 
\begin{figure}[!h]
    \centering
    \includegraphics[width=10cm]{images/implem/struct_rt.png}
    \caption{Structure interne de la table de routage}
    \label{fig:struct_rt}
\end{figure}
RPL utilise la structure \emph{rpl\_route\_entry} fournissant des informations sur l'état de l'entrée. Le lien n'est pas un pointeur, mais un accès direct à la structure. Ses variables seront décrites et utilisées dans les chapitres suivants.

\subsection{Intégration}
Dans la description RFC de 6LoWPAN-ND \cite{rfc6775}, il est question d’utiliser uniquement une \acrfull{NC}. Avec la découverte des voisins, l’hôte dialogue avec le routeur qui connaîtra au final les deux adresses de l’hôte : la locale et la globale. Ces deux adresses sont stockées dans la \acrshort{NC} avec l'\acrshort{EUI-64}. Malheureusement, Contiki ne fournit pas la possibilité d'avoir deux adresses \acrshort{IPv6} associées à une même adresse \acrshort{MAC-64} (indexations par \acrshort{MAC-64}). Il y aura donc conflit et le phénomène suivant apparaîtra: un changement constant entre ces deux adresses; switch de l’une à l’autre. En effet, en regardant la figure \ref{fig:seq_nd_6lowpan}, à l'arrivée d'un \acrshort{RS}, nous avons un changement d'une adresse globale en une locale et à la réception d'un \acrshort{NS}, une transition d'une locale en une globale.

Pour résoudre cela, et être en accord avec l'implémentation \acrshort{RPL} de Contiki, nous adaptons légèrement les instructions du \acrshort{RFC}. Lors de l'enregistrement de l'adresse locale dans le \acrshort{NC}, celle-ci y restera jusqu'au timeout. Lorsque l'adresse globale demandée par un \acrshort{NS} est correcte, elle est ajoutée dans la table de routage, au lieu du \acrshort{NC}, avec comme nexthop l'adresse locale précédemment découverte avec le \acrshort{RS}. 

Pour s'assurer qu'il n'y a pas de duplication d'adresses, tout \acrshort{6LR} envoi un \acrshort{DAR} à la réception d'un \acrshort{NS} provenant d'un \acrshort{6LH}. L'\acrshort{IPv6} demandé ne peut pas être stocké dans la \acrshort{NC} par souci de conflit. Pour résoudre le problème, nous créons une table DAR (figure \ref{fig:struct_nc}) qui possède un pointeur vers l'entrée \acrshort{NC}. Son utilité est d'avoir à disposition les informations pour parvenir à renvoyer un \acrshort{DAR} au \acrshort{6LBR} lors d'une retransmission. A la réception du \acrshort{DAC}, nous pouvons déplacer l'entrée vers la table de routage (ajout plus suppression).

\subsection{Détermination d'un routeur}
Dans chaque entrée du Neighbor Cache, nous avons un flag intitulé \textit{isrouter} (figure \ref{fig:struct_nc}). Ce flag, déterminé en partie par \acrshort{6LoWPAN-ND}, va jouer un rôle important au chapitre suivant. Pour un \acrshort{6LH}, il est très facile de déterminer sa valeur (\textit{YES}) puisqu'un hôte ne peut que se connecter aux routeurs. Concernant les \acrshort{6R}, la tâche est plus difficile. En effet, \acrshort{6LoWPAN-ND} impose qu'un \acrshort{6LR} démarre comme un hôte. Ce protocole ne permet pas de transmettre  l'identité du demandeur (si c'est un \acrshort{6LH} ou un \acrshort{6LR}) au routeur auquel on veut se connecter. Néanmoins, le demandeur se connectera toujours à un \acrshort{6R} pour acquérir son adresse IPv6. En conclusion, si un \acrshort{6R} envoie un message \acrshort{RS}, alors \textit{isrouter} est \textit{YES}; s'il reçoit un \acrshort{RS} alors la valeur est \textit{NODEFINE}.

\subsection{Etats}
L'ensemble de \acrshort{6LoWPAN-ND} est le management de \acrlong{NC} par des timers et la réception ou l'émission de messages. La figure \ref{fig:nce_in_out} résume les messages envoyés (droite) et reçus (gauche), pour les différentes entités. A chaque réception d'un message \acrshort{6LoWPAN-ND}, l'état des entrées dans la \acrshort{NC} change. Elle varie en fonction du type de message, de l'entité et le type d'adresse. Ensuite, le noeud prendra une décision engendrant un envoi d’un message, si nécessaire. Nous l'avons modélisé par la ligne grise reliant les messages entrés et sortis. 

\begin{figure}[!h]
    \centering
    \includegraphics[width=16cm]{images/implem/nce_inout.png}
    \caption{Entrées - sorties des messages 6LoWPAN-ND pour chaque entité avec leurs états}
    \label{fig:nce_in_out}
\end{figure}

L'ensemble des états dans la \acrshort{NC} a été redéfini par quatre nouveaux noms:
\begin{itemize}
\item Garbage-Collection ($GC$): L'entrée sera supprimée en cas de mémoire basse.
\item Registered ($R$): L'entrée est enregistrée avec son lifetime jusqu'à expiration.
\item Tentative ($T$): L'entrée effectue une tentative d'enregistrement avec le routeur. Elle possède un lifetime très petit.
\item Tentative DAD ($T_{DAD}$): L'entrée correspond à une tentative de duplication d'adresse auprès du \acrshort{6LBR}.
\end{itemize}

La figure \ref{fig:nce_in_out} modifie l'état de la \acrshort{NC} et définit les valeurs des timers inclus dans les messages. La gestion de cette \acrlong{NC} avec le management du temps peut être modélisée par une machine à états (figure \ref{fig:nce_st_machine}). Au centre, l’état final représente la suppression de l’entrée du cache. Les transitions sont activées par des expirations de timer (timeout). La transition de \emph{R} vers \emph{T} exprime un pourcentage avant le renvoi d'informations; nous l'aborderons plus en profondeur à la section \ref{sec:timing}. Les transitions partant de $T_{DAD}$  et $T$ vers $rm$ sont activées au nombre de renvois consécutifs d'un même message supérieur à un seuil.
Les pointillés représentent une transition prise à l'arrivée de messages ICMPv6 dans la stack uIP. Nous ne pouvons qu'atteindre l'état Registered après une interaction du réseau. 
\begin{figure}[!h]
    \centering
    \includegraphics[width=14cm]{images/implem/nce_st_machine.png}
    \caption{Machine à états d'une NCE}
    \label{fig:nce_st_machine}
\end{figure}



\section{Border Router}

\subsection{Structure}
\acrshort{6LoWPAN-ND} utilise abondamment les timers et les rafraîchit en effectuant une demande à l'entité responsable. Il existe deux types d'appareils importants. 
Le premier est le ou les routeurs auxquels les noeuds se connectent. Ils sont représentés par la structure \emph{uip\_ds6\_defrt} (default route) sur la figure \ref{fig:struct_br}. Nous nous adressons à eux pour toute les actualisations de timers.
La seconde est le \acrlong{BR}. Ils permettent de diffuser les informations à l'ensemble du réseau, comme des préfixes et informations de contexte. Celui-ci est central, car l'envoi des options \acrshort{PIO} et \acrshort{6CO} est toujours accompagné d'un \acrshort{ABRO}, désignant le \acrlong{BR}. Il se doit d'avoir un lien entre les structures représentant ces options: \emph{uip\_ds6\_prefix} et \emph{uip\_ds6\_context\_pref}, en lien avec le BR, \emph{uip\_ds6\_border\_router}.
Pour renouveler les informations diffusées, nous devons choisir une route par laquelle le BR sera atteignable. Pour ce faire, un lien subsiste de la structure \emph{defrt} vers la structure du \acrlong{BR}

\begin{figure}[!htbp]
	\centering
	 \includegraphics[width=15cm]{images/implem/struct_br.png}
	 \caption{Structure de stockage des informations}
	 \label{fig:struct_br}
\end{figure}

La figure \ref{fig:struct_br} montre que les structures sont adaptées en fonction de l'entité. Le \acrlong{BR} n'envoyant pas de \acrshort{RS} (figure \ref{fig:struct_br}), les variables de retransmission de ce message sont inutiles. Nous avons ajouté un état dans la structure default route pour pouvoir contrôler les retransmissions des \acrshort{RS} à travers ce routeur. La valeur du lifetime est l'obligation d'apparaître dans les \acrshort{6R} alors que le timer d'expiration ne doit être présent que dans les \acrshort{6LR} et \acrshort{6LH}. Le même phénomène est présent dans les préfixes et informations de contexte.



\subsection{Multi Border Router}
Notre design d'implémentation est élaboré pour accepter plusieurs \acrlong{BR}. En effet, la structure \emph{uip\_ds6\_border\_router} est utilisée pour construire un tableau. Par défaut, le nombre d'entrées est fixé à un, car il existe des problèmes pratiques relatifs à Contiki pour l'intégration de \acrshort{BR} multiples. Un développement théorique est fait en annexe \ref{sec:multi_br}. Cependant, la réalisation prendrait trop de temps à faire, car rien n'est prévu à cet effet.  Néanmoins, les spécifications imposées par le RFC de 6LoWPAN-ND  ont été faites; comme l'envoi de multi \acrshort{ABRO} aux hôtes.


\section{Renouvellement des informations}
\subsection{Timing}
\label{sec:timing}
\acrshort{6LoWPAN-ND} utilise un grand nombre de timers pour accomplir les objectifs. Ceux-ci doivent être actualisés régulièrement en procédant à l'envoi de \acrshort{RS} ou \acrshort{NS}. Le \acrshort{RFC} nous indique que les messages doivent être envoyés aux routeurs bien avant que le timer expire. Pour ce faire, nous avons deux possibilités facilement implémentables.

La première est de prendre un pourcentage du temps de vie, borné par une valeur minimum. La formule suivante représente la condition pour laquelle la procédure d'actualisation des timers peut être faite.
\begin{center}
$TIME_{remaining}(timer) < min(TIME_{lifetime}(timer)*PERCENT/100 ,DELAY_{retran}) $\\
$\Leftrightarrow$\\
$(100-PERCENT)*TIME_{remaining}(timer) < PERCENT*TIME_{elapsed}(timer)$\\
$or$\\
$ TIME_{remaining}(timer) < DELAY_{retran}$
\end{center}
Nous devons envoyer un message quand le temps restant du timer est inférieur à un pourcentage du temps total (temps écoulé + temps restant) ou que le temps restant est inférieur à un seuil. Pour notre implémentation, nous avons choisi cette expression avec un pourcentage 10\% et le seuil à 60 secondes comme imposées dans le RFC. 

D’un point de vue théorique, l'avantage de cette solution permet de moins retransmettre des messages plus les appareils sont éloignés du Border Router. En effet, un 6LR va toujours s'enregistrer auprès du 6LBR avant que le 6LH ne le fasse. Prenons par exemple la diffusion des informations de contexte, figure \ref{fig:timer_6lowpannd}. Le 6LH ne pourra avoir ces informations avant que le 6LR les ait. Les timers sont implémentés en secondes alors que les informations du 6CO sont exprimées en minutes, permettant d’avoir des arrondis. Nous utilisons le supremum de la valeur en seconde.
Le temps d'envoi du message entre senseurs permet de retarder légèrement la valeur d'expiration. Au final, nous aurons plus de chances que le 6LR actualise ses données avant que les 6LH ne le fassent. A cause de la conversion seconde en minute dans 6CO, nous sommes dans l'obligation d'avoir la valeur minimum de transmission à 1 minute. Sans retransmission à la couche physique, cette solution présente le désavantage d'avoir de potentielles collisions sur le canal et des congestions dues à l'envoi presque simultanément des messages de rafraîchissement. Ce phénomène peut diminuer l'avantage gagné.
\begin{figure}[!h]
    \centering
    \includegraphics[width=16cm]{images/6lowpannd_timing.png}
    \caption{Gestion des timers dans 6LoWPAN-ND}
    \label{fig:timer_6lowpannd}
\end{figure}


La seconde solution présente l'effet inverse. Elle propose d'utiliser une valeur aléatoire entre deux bornes: pourcentage du temps de vie et la valeur minimum de retransmission. La formule exprime la condition selon laquelle le temps restant du timer est inférieur à une valeur aléatoire entre un pourcentage du temps total et un seuil.\\
\begin{center}
$TIME_{remaining}(timer) < rand(TIME_{lifetime}(timer)*PRECENT/100, DELAY_{retran})$
\end{center}
L'ajout d'une valeur aléatoire résout le désavantage de la première solution, mais augmentera la retransmission de messages, car nous n’aurons plus cet ordre en fonction de la distance par rapport au \acrshort{6LBR}.




\subsection{Renouvellement 6CO}
L'ensemble des informations de contexte est diffusé par le \acrshort{6LBR}. Pour le modifier, nous devons supprimer l’entrée et ajouter la nouvelle version. En vue du mécanisme présenté dans cette section, une entrée libre dans la table de contexte est obligatoire pour effectuer cette opération. L'information doit se répandre de manière intelligente pour ne pas perdre le contexte alors que le réseau l’utilise  toujours. Pour ce faire, l'ajout ou la suppression d'un contexte passe par un temps d'adaptation. A l’ajout, le contexte n’est pas immédiatement utilisé pour la compression d’adresse. A la suppression, il ne sera utilisé qu’à la décompression.  La figure \ref{fig:timing_6co} nous montre les états des entrées dans la table de contexte.

\begin{figure}[!htbp]
	\centering
	 \includegraphics[width=16cm]{images/implem/timing_6co.png}
	 \caption{Etats des entrées dans la table des contextes}
	 \label{fig:timing_6co}
\end{figure}

A la réception d'un \acrshort{6CO}, le système modifie l’état selon la figure \ref{fig:timing_6co}:
\begin{itemize}
	\item Nouvelle entrée: le contexte est ajouté dans la table avec l'état \emph{ADD}. Celui-ci compresse les adresses après \emph{contexte change} secondes (MIN CONTEXT CHANGE DELAY).
	\item Rafraîchissement avec C=1: Le flag C dans le header 6CO indique si nous pouvons compresser les adresses IPv6 (valeur à 1) ou pas (valeur à 0). Etant dans l'état \emph{COMPRESS} ou \emph{SENDING,} nous replaçons le timer au début du \emph{lifetime}.
	\item Rafraîchissement avec C=0: L’état est donc \emph{UNCOMPRESS} car nous voulons uniquement décompresser les adresses IPv6 pendant le lifetime donné par 6CO.
	\item Suppression: A la demande d'une suppression d'un 6CO, nous le passons dans l'état \emph{RM} qui permet la décompression pendant un délai de \emph{MIN CONTEXT CHANGE DELAY}.
\end{itemize}
Lors de la compression (état \emph{COMPRESS}), nous regardons le temps restant avant de demander un rafraîchissement des données. Selon le comportement vu à la section \ref{sec:timing}, nous passerons dans l'état \emph{SENDING} en envoyant des messages \acrshort{RS}. Si aucune réponse n'est faite, le timer expirera et nous passerons dans le mode \emph{UNCOMPRESS}. Le délai du timer est actualisé à deux fois le \emph{lifetime} précédent. Si aucune réception n'est faite pour passer dans un autre état, l'entrée est supprimée de la table.


\section{Remarque du RFC}
L'option ABRO fournit une version sur 32 bits (figure \ref{fig:header_abro}). Ceux-ci sont divisés en 2 parties de 16 bits de poids faible suivi de 16 bits de poids fort (Version Low, Version High). Une piste d'amélioration serait d'inverser ces deux champs et d'avoir les bits High puis Low. En effet, cela permettrait de gagner en instructions-machine: décalage binaire et addition. Ce type de version laisse à penser d'avoir affaire à une représentation binaire d'un nombre à virgule fixe. Mais l'utilité de ce type de versionement n'est nullement sollicité dans le RFC.

