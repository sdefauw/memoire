\chapter{Validation}
Dans ce chapitre, nous allons élaborer toutes les techniques et méthodes de tests pour valider notre implémentation de \acrshort{6LoWPAN-ND} et son intégration avec \acrshort{RPL}. Nous développerons les tests effectués pour la non-régression de l'implémentation et les tests quantitatifs afin de déterminer les performances de notre code. 

\section{Tests de régression}
Dans le cadre de l'implémentation de \acrshort{6LoWPAN-ND}, nous sommes dans l'obligation de produire des tests pour s'assurer que l'implémentation fonctionne. Les tests de non-régression permettent de vérifier si l'intégralité du code fonctionne lors d'une modification apportée.

\subsection{Topologies}
Nos tests sont réalisés dans le simulateur Cooja \cite{contiki}. Ils s'orientent vers deux topologies différentes, auxquelles nous avons testé les principales caractéristiques de \acrshort{6LoWPAN-ND} ainsi que les situations les plus susceptibles de se produire. La première topologie (figure \ref{fig:test_topo_1}) est linéaire tandis que le deuxième (figure \ref{fig:test_topo_2}) plus complexe permet la connexion avec plusieurs routeurs.
La portée de chaque senseur est déterminée par le cercle vert. Si un autre senseur est présent dans cette zone, il recevra 100\% des messages transmis. Le cercle gris signifie que le paquet est reçu par l'appareil, mais ne peut être lu.
\begin{figure}[!htbp]
    \centering
    \subfloat[Topologie 1]{
    \includegraphics[height=7cm]{images/test/test_topo1.png}
    \label{fig:test_topo_1}
    }
    \hfill
    \subfloat[Topologie 2]{
    \includegraphics[height=7cm]{images/test/test_topo2.png}
    \label{fig:test_topo_2}
    }
    \caption{Topologie de tests comportant 6LBR (rouge), 6LR (orange) et 6LH (vert)}
    \label{fig:test_topo}
\end{figure}


\subsection{Méthodologie}
La plupart des tests effectués sont de haut niveau. Cela consiste à vérifier si l'ensemble de noeuds peut communiquer entre eux. Pour l'effectuer, nous nous assurons que l'envoi de chaque paquet \acrshort{UDP} depuis chaque \acrshort{6LH} et \acrshort{6LR} vers le \acrshort{6LBR} arrive à destination. Nous garantissons aussi le parcours inverse: du Border Router vers les noeuds.

Nous sommes descendus de niveau en vérifiant l'envoi de messages ou le contenu des différentes tables dans les senseurs. Pour accomplir ces tâches, nous avons un certain nombre de scripts (Cooja script) pour afficher des informations. Dans la version pure de \acrshort{6LoWPAN-ND}, le protocole de routage est absent; les routes doivent être entrées manuellement. L'automatisation par script fut importante, car une entrée dans la table de routage ne peut être faite qu'à condition d'avoir l'adresse nexthop dans \acrshort{NC}. 

Contiki fournit un Shell permettant d'envoyer ou recevoir des informations à travers le bus \acrfull{UART}. Ce canal permet au script de commander les senseurs. Nous avons développé un ensemble de commandes pour exécuter les tests:
\begin{itemize}
\item \emph{sendudp} : Envoi d'un paquet \acrshort{UDP} à un noeud désigné par l'adresse \acrshort{IPv6}
\item \emph{netd} : Affichage des tables: \acrlong{NC}, \acrlong{RT}, \acrlong{DADT}, Prefix Table et Context Table
\item \emph{route} : Permets l'ajout et la suppression d'entrées dans la table de routage
\item \emph{cp} : Ajout et suppression des informations de contexte dans le \acrshort{6LBR}
\item \emph{prefix} : Ajout et suppression des préfixes dans le \acrshort{6LBR}
\item \emph{reboot} : Redémarrage rapide d'un noeud 
\item \emph{unregister} : Désinscription d'un routeur
\end{itemize}

L'envoi de messages est un point important de nos tests. En effet, ceux-ci nous permettent de vérifier la convergence  du réseau. Nous considérons qu'un réseau est stable quand il n'envoie et ne reçois plus de messages pendant 60 secondes. Pour détecter la transmission de messages, nous affichons  des messages formatés sous le format spécifique à Cooja dans le "Mote output" (affichage de la sortie standard des noeuds). Une macro spécifique au test est utilisée afin de ne pas alourdir le code final. 

\subsection{Les tests}
L'ensemble de ces tests s'effectue sur la version de \acrshort{6LoWPAN-ND} pur et celle intégrée à \acrshort{RPL}.
 
\subsubsection{Topologies}
Les deux topologies présentées ci-dessus sont testées dans un environnement sans perte. La procédure consiste à effectuer 10 fois le test d'envoi de paquet \acrshort{UDP} à chaque stabilisation du réseau. Cette méthode assure le bon fonctionnement après au moins 2 cycles de rafraîchissement de l'ensemble des timers présent dans 6LoWPAN-ND. 

La première topologie, figure \ref{fig:test_topo_1}, est linéaire nous permettant de tester l'ensemble des cas simples; comme l'envoi de messages \acrshort{DAM} pour l'acquisition d'une adresse. Dans la seconde topologie, figure \ref{fig:test_topo_2}, nous effectuons un enregistrement avec plusieurs routeurs pour un même hôte. C'est le cas pour le noeud 6 avec 4 et 5 mais aussi certains \acrshort{6LR}. Prenons le cas du noeud 4, il peut effectuer un enregistrement avec 3 autres routeurs (1, 2 et 5). Il ne le effectuera pas, car le nombre est limité par une macro définie à 2. Cette condition nous permet de voir le comportement avec peu de mémoire. Remarquons que les chemins pris par les envois de \acrshort{DAR} et \acrshort{DAC} ne sont pas les mêmes. En effet, les \acrshort{DAR} utilisent la route par défaut, alors que les \acrshort{DAC} emploient la table de routage.

\subsubsection{Panne}
Le panne fait partie d'un phénomène pouvant arriver souvent avec des appareils qui fonctionnent en permanence. Le crash d'un nœud peut être considéré comme une extinction brutale: arrachage de l'alimentation, batterie hors service... Ce cas peut être assimilé à un déplacement d'un senseur de manière à ne plus être à la portée du reste du réseau. 

Un autre panne, pouvant survenir, est le redémarrage d'un noeud (carsh-recovery). L'effet engendre toute la suppression de la RAM, c'est-à-dire les données des tables. Le redémarrage peut être très bref, sans qu'aucun noeud ne puisse remarquer la disparition temporaire.

Dans le test réalisé sur un Host (\acrshort{6LH}), nous établissons la topologie 2 (figure \ref{fig:test_topo_2}) et redémarrons le noeud 6. La topologie stabilisée, nous nous assurons qu'il possède bien une adresse \acrshort{IPv6} globale et des connexions vers 4 et 5.

La première topologie est utilisée dans le test du redémarrage du Border Router. Nous reconstruisons la table de routage du noeud avec un script. Après redémarrage, le \acrshort{6LBR} perd l'ensemble de ses tables et ne sait plus dialoguer dans la direction downward. Il faut attendre que le timer dans les messages \acrshort{ABRO} expire pour que l'ensemble du réseau redevienne opérationnel. A ce moment-là, nous effectuons le test \acrshort{UDP} sur chaque noeud. 

\subsubsection{Déplacement}
Le déplacement consiste à réaliser un mouvement spatial d'un noeud dans le réseau. La simulation peut refléter le déplacement d'un senseur physique par une personne. Un obstacle temporaire entre les noeuds peut se trouver: par exemple l'ouverture d'une porte. 

Le test est réalisé avec la topologie 2 (figure \ref{fig:test_topo_2}) en déplaçant le noeud 6. Tout d'abord, nous le changeons de place pour qu'il soit hors de portée du numéro 5. Ensuite, nous le mettons à l'extérieur du réseau. Pour finir, celui-ci revient à sa place initiale. A chaque mouvement, une lecture des tables est opérée pour s'assurer que l'entrée est supprimée ou ajoutée.

\subsubsection{Préfixe}
Le préfixe est diffusé à travers les noeuds à partir du Border Router. Le préfixe permet de construire l'adresse \acrshort{IPv6} avec \acrshort{PIO} ou la compression de ceux-ci avec \acrshort{6CO}.

\paragraph{Information de préfixe}
Le test effectué consiste à construire un réseau d'adresse avec un préfixe bbbb::/16. Stabilisé, nous effectuons un changement dans le Border Router en remplaçant bbbb::/16 par cccc::/16. L'objectif est d'attendre que l'ensemble des adresses change en contrôlant leurs valeurs avec Cooja script. Finalement, nous vérifions la connectivité en envoyant des messages \acrshort{UDP}. 

\paragraph{Information de contexte}
Le test est de diffuser le contexte bbbb::/16 et le changer par cccc::/16 à la stabilisation du réseau. Nous étudions les comportements dans chaque appareil décrit à la figure \ref{fig:timing_6co}.
A chaque envoi de \acrshort{RS}, les entrées dans la table de contexte sont vérifiées:  d'abord les deux préfixes puis uniquement le  nouveau.

\subsubsection{Duplication d'adresse}
La vérification de l'implémentation de duplication d'adresse s'impose. Comme nous utilisons des adresses GP64, nous devons créer $2^{64}$ noeuds; ce qui est irréalisable. Pour ce faire, nous avons inventé un nouveau type de noeud où la fonction d'auto-configuration de l'adresse \acrshort{IPv6} est réécrite. Elle génère toujours la même adresse. Lors de la simulation, le \acrlong{BR} reçoit les mêmes valeurs des \acrshort{DAC} venant de ce type de noeud. Le premier à s'enregistrer gagne.

\subsubsection{Dé-enregistrement}
La spécification de \acrshort{6LoWPAN-ND} impose la possibilité de se désinscrire d'un routeur. Sur la deuxième topologie (figure \ref{fig:test_topo_2}), le noeud 6 établit une connexion avec le 4 et 5. L'objectif du test est de vérifier la suppression de l'entrée dans le \acrshort{NC} lors de l'envoi du message de dé-enregistrement. Ce dernier consiste à envoyer un \acrshort{NS} avec un lifetime à zéro dans l'option \acrshort{ARO}. La destination est le routeur auquel l'on veut se désabonner.




\section{Compatibilité du standard}
\label{sec:standard}
Wireshark \cite{wireshark} est un analyseur de paquets de réseaux informatiques. Il permet de "lire" un grand nombre de protocoles réseau, dont \acrshort{6LoWPAN-ND}. Les fichiers de capture \emph{pcap} peuvent être lus par le logiciel. Il les affiche et les formate dans une interface graphique. Le simulateur Cooja permet de générer des fichiers \emph{pcap} du trafic simulé. Nous pouvons visualiser facilement l'échange des paquets et leurs conformités au standard. En effet, le développement de Wireshark est indépendant du travail réalisé ici, nous garantissant la même compréhension du respect du format des messages par rapport au RFC.

\begin{figure}[!h]
    \centering
    \includegraphics[width=10cm]{images/analyse/wireshark_sc.png}
    \caption{Scénario de validation du standard}
    \label{fig:wireshark_sc}
\end{figure}


Nous avons créé un scénario (figure \ref{fig:wireshark_sc}) représentant l'ensemble des header utilisé dans le protocole \acrshort{6LoWPAN-ND}. Le \acrshort{6LR} demande l'acquisition d'une adresse IPv6 globale au \acrshort{6LBR}, suivi du \acrshort{6LH} au \acrshort{6LR}. 

La figure \ref{fig:wireshark_process} montre l'ensemble de cette interaction simulée avec Cooja. La première partie de l'interaction \acrshort{6LR} -  \acrshort{6LBR} est représentée par les paquets numérotés de 3 à 7. La deuxième partie est constituée des paquets 8 à 14. La fragmentation est opérée pour les messages RA  car il faut deux paquets pour leurs envois. Les paquets 1 et 2 sont des \acrshort{RS} venant de \acrshort{6LH}. Ceux-ci n'aboutissent à aucune réponse de la part du \acrshort{6LR}, car ce dernier n'est pas encore un routeur (pas d'IPv6 globale acquise). 
L'ordre de démarrage des noeuds a de l'importance sur le nombre de messages transmis. Au sein de Cooja, cet ordre est déterminé par un nombre aléatoire: \emph{RANDOMSEED}. La figure \ref{fig:wireshark_sc} nous montre aussi le rafraîchissement des timers de \acrshort{6LH}.

\begin{figure}[!h]
    \centering
    \includegraphics[width=15cm]{images/analyse/wireshark/interaction.png}
    \caption{Trace wireshark du scénario de validation du standard}
    \label{fig:wireshark_process}
\end{figure}

A travers les captures de la section \ref{option_capture}, nous observons l'ensemble des options dédiées à \acrshort{6LoWPAN-ND} pour les différents types de messages. Nos formats sont effectivement conformes à la compréhension qu'a faite Wireshark sur ces options.




\section{Tests quantitatifs}
Dans cette section, nous présentons des tests et des mesures pour  valider les performances de notre implémentation. Nous abordons les performances mémoires, le temps de démarrage et le nombre d'échanges de données dans différentes topologies.

\subsection{Performance Mémoire}
\label{sec:preform_memory}

Le paragraphe suivant conserve la répartition de mémoire utilisée sur un "Sky".  Ce type d'appareil est très limité en capacité mémoire et  susceptible d'être utilisé pour les routeurs périphériques. Chacun d'eux est constitué d'un emplacement mémoire ROM pour stocker le code assembleur et d'un emplacement réservé aux variables: RAM. La frontière entre les deux est délimitée par un adressage mémoire fixe rendant la capacité de 48KB pour la ROM et 10KB pour la RAM. La figure \ref{fig:analyse_memory} représente ces deux valeurs par des barres horizontales. Pour que chaque entité puisse être placée dans un Sky, nous avons recours à l’usage de "nullrdc\_driver"  à la place de "contikimac\_driver" pour réduire la couche mac (stack uIP).

L'ensemble des valeurs obtenues sur la figure \ref{fig:analyse_memory} provient de la commande "msp430-size" avec le binaire généré par le compilateur "msp430" ciblé pour les appareils Sky. Les valeurs chiffrées sont disponibles dans le tableau \ref{tab:memory} en annexe.

\begin{figure}[!h]
    \centering
    \includegraphics[width=15cm]{images/analyse/memory.png}
    \caption{Taille de la mémoire ROM et RAM utilisée pour chaque entité 6LoWPAN-ND}
    \label{fig:analyse_memory}
\end{figure}

La capacité utilisée par le code source avec le protocole de routage \acrshort{RPL} est plus importante comparé à une utilisation en un routage manuel avec \acrshort{6LoWPAN-ND} (pur). Chaque entité \acrshort{6LoWPAN-ND} n'est pas demandeuse de la même quantité de mémoire. Conformément à nos attentes, \acrshort{6LH} réalise le meilleur score et confirme sa position à être un senseur périphérique. \acrshort{6LBR} requiert moins de ressources que \acrshort{6LR}, car ce dernier doit fournir des services communs à \acrshort{6LBR} et \acrshort{6LH}. Dans notre test, le \acrshort{6LBR} n'est qu'un nœud et ne possède pas de protocole pour dialoguer avec le Backbone (figure \ref{fig:wsn}). En effet, l'implémentation des propositions faites en annexe \ref{sec:multi_br} augmentera l'utilisation de la mémoire. L'ajout du protocole \acrshort{RPL} à \acrshort{6LoWPAN-ND} accroît l'usage de la ROM dû à son implémentation. 

L'entité \acrshort{6LR}+\acrshort{RPL} tient toujours dans un "Sky" mais frôle sa limite physique.  Si nous utilisons la couche "contikimac\_driver", ce dernier doit utiliser un autre type d'appareil avec plus de mémoire. Pour cette raison,  l'ensemble de nos tests avec \acrshort{RPL} est exécuté avec des "wismote". Ils possèdent assez de mémoire pour ajouter par dessus des applications telles qu'un Shell, indispensable pour nos tests quantitatifs et de régression.

Au regard de la mémoire RAM, il existe deux types de variables: les non initialisés avec des emplacements mémoire nulle et inversement, des variables initialisées avec une valeur attribuée dans le code source. L'usage de cette mémoire est croissant à partir \acrshort{6LH} jusqu'à \acrshort{6LBR}. Ce dernier demande plus de ressources en raison des tables supplémentaires; telles que la table de duplication d'adresse. \acrshort{6LR} est au milieu, car il possède des variables supplémentaires à \acrshort{6LH}, comme des paramètres de diffusions (figure \ref{fig:struct_br}); et n'a guère besoin d’ajouter de nouvelles tables. Si le protocole de routage est présent, nous avons une nécessité d'accroître la mémoire RAM due à son implémentation.




\subsection{Performance Nexthop}
Les tests réalisés dans cette section permettent l'évaluation des performances Nexthop. Pour ce faire, nous utilisons une topologie "linéaire" (figure \ref{fig:topo_nexthop}) simulant la profondeur d'un graphe directement lié au nombre de Nexthop. Aux extrémités, nous avons un \acrlong{BR} et un hôte. Entre ces deux entités, nous plaçons un nombre variable de \acrshort{6LR}. Chaque senseur ne peut voir que son voisin direct; de gauche et de droite le plus proche (deux voisins au maximum).

\begin{figure}[!h]
    \centering
    \includegraphics[width=7cm]{images/analyse/topo_nexthop.png}
    \caption{Topologie linéaire de tests de performance Nexthop}
    \label{fig:topo_nexthop}
\end{figure}

\subsubsection{Démarrage}
Nous prenons en compte deux facteurs importants au démarrage d'un réseau \acrshort{WSN}: le temps de convergence du réseau et le nombre de messages envoyés ou reçus.

\medskip
Le facteur temps est le nombre de secondes qu'il faut depuis l'origine de la simulation jusqu'à l'absence d'envoi ou de réception de message pendant 30 secondes. A ce moment, nous nous assurons la présence de l’ensemble des senseurs dans la table de routage du \acrlong{BR}.

\begin{figure}[!h]
    \centering
    \includegraphics[width=15cm]{images/analyse/line_time.png}
    \caption{Temps de convergence au démarrage}
    \label{fig:topo_nexthop_time}
\end{figure}

La figure \ref{fig:topo_nexthop_time} rapporte les temps de convergence du réseau dans cette topologie. L'observation notable provient du non-alignement des points. Cela est dû à l'ordre de démarrage des noeuds au sein de Cooja. En effet, le cas idéal serait qu'ils démarrent  par rapport à l'ordre croissant de la numérotation de la figure \ref{fig:topo_nexthop}, c'est-à-dire, en premier le \acrshort{6LBR} et en dernier le \acrshort{6LH}. Si l'un d'entre eux démarre avant, il enverra un RS qui n'aboutira à rien (section \ref{sec:standard}), car le routeur parent n'est pas encore configuré comme un routeur \acrshort{6LoWPAN-ND}. Nous devrons attendre que celui-ci retransmette un nouvelle RS pour peut-être obtenir en retour un RA. Si ce routeur a des enfants, ils doivent également attendre. Nous avons une accumulation de temps. Néanmoins, nous remarquons qu'une interpolation linéaire est possible, d'où une évolution linéaire.

Pour la courbe "no RPL", nous observons un phénomène lié à la procédure de retransmission de message \acrshort{RS}: Truncated Binary Exponential Backoff. De l'origine jusqu'à 4, la courbe suit une exponentielle croissante. Pour les valeurs suivantes, le mécanisme atteint son seuil (Truncated), une linéarité apparaît. Un autre phénomène est fortement marqué sur la courbe avec routage RPL. Cette courbe suit en faisant certains pics: de 6 à 7, 10 à 11 et de 12 à 13. Cela vient de la retransmission de NA-NS qui se déroule toutes les 4 minutes (retransmission $5min - 10\% = 240s$ ).

Nous observons que la pente avec un système de routage manuel est plus faible par rapport à la prise en charge du protocole RPL. En effet, le phénomène décrit par la figure \ref{fig:problem_dar} explique cette différence notable entre les deux courbes. Il faut attendre que RPL ajoute les routes downward, pouvant demander un délai d'attente supplémentaire.

Cette linéarité nous garantit que l'installation d'une topologie \acrshort{WSN} pourra se faire en un temps raisonnable même si la profondeur du graphe est  élevée. 


\medskip
Le second facteur est le nombre total de messages caractérisés par son type et par l'envoi ou la réception. Nous les additionnons pour chaque noeud de la topologie. 

\begin{figure}[!h]
    \centering
    \includegraphics[width=15cm]{images/analyse/line_nd6.png}
    \caption{Nombre de messages ND6 envoyés ou reçus au démarrage}
    \label{fig:topo_nexthop_nd6}
\end{figure}

La figures \ref{fig:topo_nexthop_nd6} illustre le nombre de messages réceptionnés ou envoyés au niveau \acrshort{6LoWPAN-ND} dans le stack uIP de Contiki (messages ND6). 


Nous observons une différence croissante entre la courbe de réception et d'envoi, plus le nombre de nexthop est élevé. Dans le cas de \acrshort{6LoWPAN-ND}, à routage manuel, cela provient de l'envoi et de la retransmission mulicast \acrshort{RS}. En effet, les courbes de réception totalisent deux messages reçus alors qu'un seul message est envoyé, car nous avons deux voisins. Elles possèdent donc une composante quadratique par rapport aux courbes de réception pour une valeur supérieure à 3.

%TODO no linéarité de la courbe sent: pq 
%La courbe d'envoi n'est pas linéaire car il existe de la retransmission de message \acrshort{RS}. En effet
%Toutefois cette non linéarité est a relativisé car nous pouvons le voir sur la figure, l'évolution n'est pas fugurante. Il faudrait avoir une très grand nombre de nexthop avant que le nombre de messages 

Les courbes avec protocole de routage RPL sont décalées par rapport aux courbes sans. Cette offset ne prend forme qu'à partir de 4 noeuds. En effet, \acrshort{6LoWPAN-ND} peut gérer une topologie avec 3 noeuds sans protocole de routage. Dans ce cas, le \acrlong{BR} n'a besoin que des routes downward pour répondre aux \acrshort{DAR}. Les messages upward sont transmis par la route par défaut. Le \acrshort{6LBR} ne saura pas communiquer avec  le \acrshort{6LH}, mais il n'en a pas besoin pour l’utilisation de \acrshort{6LoWPAN-ND}. Dans la topologie en ligne avec 4 noeuds, le \acrshort{6LR} le plus éloigné est à 2 nexthop du \acrlong{BR}. Cela implique qu'il faut une route dans \acrshort{6LBR}. Avec RPL, nous devons attendre un certain laps de temps,  pouvant provoquer de la retransmission (section \ref{sec:insistance_ns}).

\subsubsection{Utilisation régulière}
Le comportement du réseau après démarrage est tout aussi important. Nous présenterons dans cette partie le comportement des senseurs sur l'envoi et la réception de messages au cours d’une heure après la convergence du \acrlong{WSN}.

\begin{figure}[!h]
    \centering
    \includegraphics[width=15cm]{images/analyse/line_1_nd6.png}
    \caption{Nombre de messages ND6 envoyés ou reçus pendant une heure}
    \label{fig:topo_nexthop_1_nd6}
\end{figure}
En observant la figure \ref{fig:topo_nexthop_1_nd6}, nous déduisons que les courbes de réception et d'envoi sont confondues. Cela signifie qu'il n’y a aucune perte de message ND6 et que l'ensemble des messages se fait en unicast. En effet, l'interaction RS-RA et NS-NA est en unicast

Nous constatons une transition importante du passage de 2 à 3 noeuds. L’introduction d’un \acrshort{6LR} provoque l'obligation d'utiliser les messages \acrshort{DAM} pour la duplication d'adresses. 

Les courbes entre no-RPL et RPL sont proches l'une de l"autre, car l'ajout RPL n'a pas d'influence sur \acrshort{6LoWPAN-ND}. Néanmoins, une légère différence apparaît pour un nombre important de Nexthop. 
Cela provient du fait que RPL ne met pas à jour à temps les routes et qu'elles sont supprimées. Cela aura pour conséquence la retransmission d'informations au niveau \acrshort{6LoWPAN-ND}.

\begin{figure}[!h]
    \centering
    \includegraphics[width=15cm]{images/analyse/line_1_ip_recv.png}
    \caption{Nombre de messages IP reçus pendant une heure}
    \label{fig:topo_nexthop_1_ip_recv}
\end{figure}

La figure \ref{fig:topo_nexthop_1_ip_recv} désigne l'ensemble des messages reçus au niveau IPv6 de la stack uIP de Contiki. Les valeurs affichées comprennent les paquets IPv6 atteignant la destination, mais aussi ceux qui sont forwardés. La courbe reste linéaire, mais sa pente change par rapport au message ND6, car nous ajoutons le forwarding. En effet, l'interaction NS-DAM-DAR-NA sera le double du nombre de nexthop que sépare le noeud du \acrlong{BR}.

Le protocole RPL envoie des messages supplémentaires au sein du réseau de type multicast et unicast. Avec deux voisins directs et le multicast, la courbe de réception avec RPL évoluera de manière quadratique. Le protocole de routage est uniquement présent dans les \acrshort{6R}. La distinction entre les deux courbes ne se fait qu'à partir de 3 noeuds, à cause de la présence d’au moins un \acrshort{6R}.




\subsection{Performance Densité}
Cette section nous analysera l'influence de la densité des noeuds sur les performances du réseau. Pour ce faire, deux expériences sont réalisées à partir d'une topologie "carrée" (figure \ref{fig:topo_grid}) ou appelée "grille". Nous plaçons aux extrémités opposées le \acrshort{6LBR} et \acrshort{6LH}. L'ensemble des autres noeuds pour construire la grille est constitué de \acrshort{6LR}. La radio entre la longueur et la largeur est de 1:1, signifiant que nous allons générer des topologies de 4, 9, 16 et 25 noeuds.

\begin{figure}[!h]
    \centering
    \includegraphics[width=5cm]{images/analyse/topo_grid.png}
    \caption{Topologie grille de tests de performance de densité}
    \label{fig:topo_grid}
\end{figure}

Dans la première expérience, basse densité, chaque noeud peut exclusivement voir ses 4 voisins. Sur la figure \ref{fig:topo_grid}, cela signifie que le noeud 11 ne peut que communiquer qu'avec 7, 10, 12 et 15. Dans le second test, haute densité, chaque noeud peut dialoguer avec les 8 voisins les plus proches. Sur la figure \ref{fig:topo_grid}, le noeud 11 communique avec le 6, 7, 8, 10, 12, 14, 15 et 16.
Le choix de ce ratio nous garantit le plus grand nombre de noeuds avec un maximum de voisin. En effet, nous minimisons le nombre de noeuds de bord. Nous trouvons cela en annulant le premier dérivé : $(2*(l+L-2) - l*L)’ =  0  \Leftrightarrow
 l=L$, confirmant le ratio "carré".

Nous avons voulu faire cette expérience avec un routage manuel. Elle a abouti à un échec, car en absence de protocole de routage il est très difficile de réaliser ce test. Nous avons cherché à automatiser l'insertion de routes dans chaque noeud à la configuration de chacun d'entre eux. Les phénomènes de démarrages nous empêchent de trouver une généralisation d'insertion. En effet, le démarrage de chaque machine est fait de manière aléatoire. Après de multiples tentatives, nous sommes parvenus à la conclusion suivante: le seul moyen efficace est de reconstruire la route downward à partir du suivi de la route upward. Un tel problème est complexe à mettre en place au sein de Cooja et n'importe guère de complément par rapport à la solution avec \acrshort{RPL}. Nous choisirons donc de faire nos tests uniquement avec l'implémentation de \acrshort{6LoWPAN-ND} lié au protocole de routage \acrshort{RPL}.

\medskip
L'ensemble des résultats pour la topologie traité suit les mêmes phénomènes que ceux décrits dans la partie Nexthop. Cependant, certaines parties diffèrent.

\begin{figure}[!h]
    \centering
    \includegraphics[width=15cm]{images/analyse/grid_time.png}
    \caption{Temps de convergence au démarrage pour la topologie en grille}
    \label{fig:topo_grid_time}
\end{figure}
Le temps de convergence du \acrshort{WSN} de la topologie en grille (figure \ref{fig:topo_grid_time}) est nettement en dessous de celle linéaire (figure \ref{fig:topo_nexthop_time}) avec le même nombre de noeuds. Nous observons un gain de plus de 50\% par rapport à la courbe basse densité. L'explication provient de la multiplication par deux du nombre de voisins. Ce gain ne se retrouve pas entre les deux courbes de différentes densités, mais reste conséquent. Le traitement et la propagation de l'information sont les facteurs influençant ce pourcentage. En effet, si nous poussons le raisonnement à l'extrême, dans une topologie où tous les hôtes sont directement connectés au \acrshort{6LBR},  cela signifierait que le temps de convergence serait le même, peu importe le nombre de \acrshort{6LH}.

Le nombre de messages est en moyenne le même dans les deux topologies testées (figure \ref{fig:topo_grid_nd6} et \ref{fig:topo_nexthop_nd6}). Pour 9 noeuds, nous avons plus ou moins 100 messages ND6 envoyés et reçus. Nous noterons une différence légèrement supérieure pour la basse densité.
\begin{figure}[!h]
    \centering
    \includegraphics[width=15cm]{images/analyse/grid_nd6.png}
    \caption{Nombre de messages ND6 envoyés ou reçus au démarrage pour la topologie en grille}
    \label{fig:topo_grid_nd6}
\end{figure}

Les simulations sur 1 heure (figures \ref{fig:topo_grid_nd6}) rapportent des résultats similaires à ceux précédemment présentés. Les courbes sont très proches l'une de l'autre.
\begin{figure}[!h]
    \centering
    \includegraphics[width=15cm]{images/analyse/grid_1_nd6.png}
    \caption{Nombre de messages ND6 envoyés ou reçus pendant une heure  pour la topologie en grille}
    \label{fig:topo_grid_nd6}
\end{figure}


\FloatBarrier

\subsection{Performance des optimisations}
Les optimisations décrites à la section \ref{sec:opti} et implémentées dans Contiki sont analysées ici. Nous montrons les résultats obtenus par rapport aux attentes émises précédemment. 
L'analyse est faite sur chacune des optimisations.
Le choix de topologie de test est celui utilisé dans l'analyse Nexthop. En effet, c'est elle qui nous donne les résultats les plus critiques.

\begin{figure}[!h]
    \centering
    \includegraphics[width=15cm]{images/analyse/opti_time.png}
    \caption{Temps de convergence au démarrage avec optimisation}
    \label{fig:topo_opti_time}
\end{figure}


\begin{figure}[!h]
    \centering
    \includegraphics[width=15cm]{images/analyse/opti_msg.png}
    \caption{Nombre de messages ND6 envoyés ou reçus au démarrage avec optimisation}
    \label{fig:topo_opti_nd6}
\end{figure}


\subsubsection{Démarrage RPL}
Nous voyons sur la figure \ref{fig:topo_opti_time} qu'il existe une différence sur le temps de démarrage en faveur de cette optimisation. 
Comme expliqué dans l'optimisation, le délai d'attente entre \acrshort{6LoWPAN-ND} et \acrshort{RPL} est diminué, réduisant le temps de convergence. 
Du point de vue du nombre de messages ND6 reçus (figure \ref{fig:topo_opti_nd6}), nous voyons un net changement. En effet,  \acrshort{RPL} n'a pas le temps d'envoyer à répétition des messages DIS et DIO en multicast.

\subsubsection{Insistance NS}
Par défaut, nous avons implémenté une retransmission de messages NS  à délai fixe sans limite sur le nombre; uniquement pour la première acquisition de l'adresse IPv6. Dans l'optimisation, nous utilisons une retransmission selon une Truncated Binary Exponential Backoff. Elle nous donne un délai de convergence (figure \ref{fig:topo_opti_time}) plus élevé dû à l'agrandissement du temps entre deux retransmissions. Le nombre de messages reçus (figure \ref{fig:topo_opti_nd6}) est identique, car le nombre de rediffusions reste le même.


\subsubsection{Table de duplication}
Bien que nous ayons implémenté cette optimisation, il nous est difficile de tester son efficacité réelle. Contiki ne permet pas de connaître l'espace mémoire RAM à l'exécution. Celui-ci est fixé au démarrage par le compilateur dans une plage d'adressage. 


\subsubsection{Fusion DAD - DAO}
Pour cette optimisation, contrairement à nos attentes, le temps de convergence n'a pas diminué dans tous les cas. En effet, pour qu'un DAO avec l'option DARO soit envoyé, il faut qu'il possède un parent. Pour ce faire, \acrshort{RPL} doit avoir exécuté l'interaction DIO - DAO au \acrshort{6LR} envoyeur. Il faudra un délai d'attente entre l'optimisation et le protocole de routage, augmentant le temps de démarrage (figure \ref{fig:topo_opti_time}). Précédemment, nous avions une différence de temps entre les protocoles (\acrshort{6LoWPAN-ND} - \acrshort{RPL}), maintenant il incombe à \acrshort{RPL} seul le délai entre les DIO - DAO et l’envoi de l’option DARO. Par conséquent, certaines valeurs sont meilleures et d'autres sont inférieures par rapport à la courbe sans optimisation.
Le nombre de messages (figure \ref{fig:topo_opti_nd6}) est légèrement inférieur, car certains messages DIS multicast ont disparu, en raison de l’envoi d’un DAO à la place d'un DAR.

%TODO graphe longue durée


%\subsubsection{Performance globale}
%TODO


\subsection{Conclusion}
Comme nous avons pu le voir dans cette analyse quantitative, le protocole \acrshort{6LoWPAN-ND} possède des avantages du point de vue mémoire. Il permet la réutilisation d'appareils qui sont à leurs limites de capacité avec \acrshort{RPL} pur. 
L'ajout du protocole \acrshort{RPL} sur  \acrshort{6LoWPAN-ND} a une influence sur les performances globales. Malgré tout, celui-ci apporte une simplicité de configuration de la table de routage et une appréhension aux changements. En vue des résultats, nous recommandons d'avoir un maximum d'appareils périphériques (\acrshort{6LH}) reliés à une grappe de \acrshort{6R}. Si ce regroupement de routeurs est simple et que nous sommes soucieux de préserver l’énergie,  nous recommandons un routage manuel. Dans le cas contraire, le routage avec RPL est un choix judicieux par la complexité de mettre en oeuvre une configuration manuelle. L'utilisation des optimisations pourra atténuer l'effet de l'ajout de RPL, mais dépendra du choix de la topologie de déploiement de senseurs.

