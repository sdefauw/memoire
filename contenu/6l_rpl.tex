
\chapter{RPL et 6LoWPAN-ND}

L'objectif final du travail est de faire fonctionner \acrshort{RPL} sur \acrshort{6LoWPAN-ND} avec le moins d'interaction possible. Comme dans un réseau "traditionnel", nous avons des routeurs avec un protocole de routage et des hôtes qui se connectent à ceux-ci. Les hôtes n'interagissent en rien au point de vue du routage avec le ou les routeurs connectés.

L'objectif est le même, nous cherchons à utiliser le protocole de routage \acrshort{RPL} pour les noeuds de types \acrshort{6LR} et \acrshort{6LBR} sans qu’il ne soit présent dans les noeuds hôtes (\acrshort{6LH}). Nous faisons cela pour des raisons de capacité des senseurs et pour leur permettre de s'endormir facilement. De plus, la mémoire des \acrshort{6LH} sera beaucoup plus petite par rapport au \acrshort{6R} en raison de l'absence de protocoles de routage. Ainsi nous aurons deux types d'appareils: des ultras légers pour les \acrshort{6LH} et des plus conséquents pour les types routeurs.




\section{RPL pur}
\acrshort{RPL} pur signifie ici qu'il n'y a pas de découverte des voisins, c'est \acrshort{RPL} qui s'occupe de construire les adresses, de modifier la \gls{NC} et de modifier les tables de routage dans les senseurs (noeuds du réseau 802.15.4). Dans l'implémentation de ce protocole de routage, au sein de Contiki, les adresses \acrshort{IPv6} locales des noeuds voisins sont stockées dans la \acrshort{NCE}. Elles sont liées à l'\acrshort{EUI-64}. Quant à l'adresse globale, elle est enregistrée dans la table de routage avec comme nexthop l’adresse locale. En effet, comme dit précédemment, Contiki indexe la \acrlong{NC} avec sa MAC-64 rendant impossible la création de deux \acrshort{NCE} liés à un même \acrshort{EUI-64}.




\section{RPL sur 6LoWPAN-ND} 
Si \acrshort{RPL} doit interagir avec \acrshort{6LoWPAN-ND}, il se doit de supprimer tous les systèmes redondants: l'envoi de préfixes et l'ajout d'entrées dans la \acrshort{NC}.

Les \acrshort{6LH} ne connaissant pas le protocole de routage, ils ne pourront pas informer les routeurs qu'ils sont présents sur le réseau au point de vue routage; c'est la tâche du routeur en liaison directe. Pour cela, ce dernier possède l'ensemble des adresses \acrshort{IPv6} globales de ses voisins dans sa table de routage et \acrshort{NC}. Il lui suffit d'annoncer au routeur parent, avec un \acrshort{DAO}, les hôtes voisins. Pour cela, le routeur doit savoir faire la distinction entre les entrées de types routeurs et hôtes dans la RT.

Nous disposons d'un flag \emph{isrouter} (par défaut, il est un hôte) dans la \acrshort{NC}. Sachant que chaque entrée dans la \acrshort{RT} a comme nexthop (adresse locale) une entrée correspondante dans la \acrshort{NC}, nous pouvons parvenir à une solution. Néanmoins, un \acrshort{6LR} démarre comme un \acrshort{6LH}, impliquant que le routeur recevant les RS ne peut savoir s'il est routeur ou hôte. 

\acrshort{6LoWPAN-ND} ne permet pas de résoudre cela, il en incombe à \acrshort{RPL} de prendre en charge ce rôle. A chaque réception d'un message de contrôle \acrshort{RPL}, nous disposons de l'expéditeur. De ce fait, nous savons que c'est un routeur. Si l'adresse est dans la \acrshort{NC}, nous sommes à même de changer le flag (passage à \emph{YES}). Pour diminuer la possibilité d’avoir de faux positifs (déclaré hôte, c’est un router), nous mettons ce flag en mode indéterminé. Quand le routeur reçoit un \acrshort{DIO} de son parent, \acrshort{6LR} envoie un \acrshort{DIO} à toutes les entrées détenant le flag indéterminé. S'il n'y a pas de réponses dans un délai défini, nous les déclarons comme des hôtes. En effet, les \acrshort{NCE} nous "assurent" que l'hôte est bien connecté à ce routeur. 

Avec ce type de mécanisme, un \acrshort{6LR} peut être désigné comme un hôte par ses voisins. En effet, lors du démarrage, tout noeud \acrshort{6LoWPAN-ND} est un \acrshort{6LH}. Donc, à chaque réception d'un message \acrshort{RPL}, nous nous assurons que l'état du flag est bien en routeur dans l'entrée de la \acrshort{NC}.


Chaque entrée dans la table de routage possède une durée de vie utilisée par \acrshort{RPL}. Comme \acrshort{6LoWPAN-ND} utilise en partie la \acrshort{RT} et le \acrshort{NCE}, nous avons des conflits de timers. La solution consiste à donner la priorité à \acrshort{6LoWPAN-ND} en utilisant la variable \emph{learned\_from} (figure \ref{fig:struct_rt}).
Si la route a été ajoutée par ce protocole, alors elle est taguée comme \emph{RPL\_ROUTE\_FROM\_6LOWPANND} et ne peut être supprimée par \acrshort{RPL}







\section{Optimisation}
\label{sec:opti}
\acrshort{6LoWPAN-ND} a pour objectif de réduire un maximum de contraintes imposées à des senseurs basse consommation. Nous avons comme objectif de réduire le trafic réseau pour abaisser la consommation d'énergie. La mémoire est un objectif crucial pour les \acrshort{LLN} que nous traitons dans cette section. Bien que ces améliorations ne sont plus conformes aux spécifications des \acrshort{RFC}, celles-ci peuvent faire l'objet d'étude afin d'accroître les lignes directives que forment les \acrlong{LLN}.



\subsection{Démarrage RPL}
\label{sec:startrpl}
L'objectif de cette optimisation est de démarrer le protocole de routage \acrshort{RPL} uniquement quand le noeud possède une adresse \acrshort{IPv6} globale. Nous aurions donc au démarrage du senseur le protocole \acrshort{6LoWPAN-ND} qui permet l'obtention d’une adresse \acrshort{IPv6} globale. A la réception d'un NA positif, nous pouvons activer la procédure d'initialisation de \acrshort{RPL}. 

Le gain se fait au point de vue du nombre de messages \acrshort{RPL} envoyés. Bien entendu, sans cette solution, les noeuds parents envoient régulièrement des messages \acrshort{DIO} aux enfants. Ceux-ci vont les recevoir, car \acrshort{RPL} est activé, mais le renvoi d'un message \acrshort{DAO} ne s'effectue pas, car ils n'ont guère à leurs dispositions qu’une adresse \acrshort{IPv6} globale. 

De plus, le délai d'attente pour l'initialisation du réseau, c'est-à-dire, le démarrage de chaque noeud et la convergence du réseau (\acrshort{6LoWPAN-ND} et \acrshort{RPL}), sera plus rapide. En effet, lorsque l'adresse est acquise, il faudra un certain délai avant de recevoir d'un message \acrshort{RPL}. Celui-ci peut être très long impliquant un temps supérieur pour le démarrage du réseau. A l'initialisation de \acrshort{RPL}, le message DIS est envoyé, forçant ainsi l'interaction DIO - DAO et réduisant ce temps.

La macro: \emph{CONF\_6LOWPAN\_ND\_OPTI\_START} est utilisé pour activer l'implémentation. Cette optimisation est uniquement applicable à \acrshort{6LR}.

\subsection{Insistance NS}
\label{sec:insistance_ns}
Le but est d'envoyer plus de messages pour accroître le délai d'initialisation et potentiellement réduire le nombre total de messages. L'objectif est d'envoyer un nombre illimité de messages \acrshort{NS} pour découvrir le réseau sans avoir le seuil des 3 retransmissions par défaut (\emph{MAX\_RTR\_ADVERTISEMENTS}). Le noeud les envoie jusqu'à l'obtention de l'adresse \acrshort{IPv6} globale. Après l'acquisition, le nombre de retransmissions redevient la valeur par défaut.

\begin{figure}[!h]
    \centering
    \includegraphics[width=10cm]{images/optimisation/problem_dar.png}
    \caption{Problème d'envoi de message DAC}
    \label{fig:problem_dar}
\end{figure}
Cette procédure évite un phénomène de problème d'envoi au démarrage. Prenons la situation (figure \ref{fig:problem_dar}) où nous avons un \acrshort{6LH} qui veut se connecter à un \acrshort{6LR}. Ce dernier n'est pas à portée du 6LBR, mais doit passer par au moins une autre \acrshort{6LR}. L'hôte envoie un message \acrshort{NS} au \acrshort{6LR} pour vérifier unicité de son adresse. Un message \acrshort{DAR} est envoyé au \acrshort{6LBR} en passant à travers un certain nombre de nexthops qui le transmettront par leur route par défaut. \acrshort{6LBR} traite l'information et essaie de transmettre un \acrshort{DAC}. Si le protocole \acrshort{RPL} n'a pas encore ajouté la route vers le \acrshort{6LR} demandeur au sein de \acrshort{6LBR}, il sera dans l'incapacité de trouver le nexthop et d'envoyer le \acrshort{DAC}. Il faudra attendre que \acrshort{RPL} modifie la table de routage, ce qui peut prendre plus de temps que l'envoi de 3 retransmissions. Le \acrshort{6LH} ne peut donc obtenir l'adresse et doit attendre que le timer du Border Router expire; attendre une nouvelle retransmission \acrshort{RS} - \acrshort{RA}. Par conséquent, l'initialisation du réseau peut prendre des heures en fonction de la profondeur du \acrshort{DODAG}.

Le désavantage principal de cette optimisation est la retransmission infinie. En effet, si un noeud est à portée d'un 6LR, l'établissement \acrshort{RS} - \acrshort{RA} s’effectue. Ensuite, s'il se retire de la portée du routeur, les messages \acrshort{NS} seront envoyés pour rien, utilisant de l'énergie inutilement. Nous observerons aussi un accroissement de l'envoi de messages \acrshort{DAR} dans le réseau.

Une solution alternative est d'utiliser le même mécanisme qu'emploie la retransmission \acrshort{RS} dans \acrshort{6LoWPAN-ND}: Truncated Binary Exponential Backoff. Nous aurons donc un compromis entre la retransmission intensive et les délais trop importants.

La macro \emph{CONF\_6LOWPAN\_ND\_OPTI\_NS} active l'implémentation et est applicable à toutes les entités \acrshort{6LoWPAN-ND}. 


\subsection{Table de duplication}
L'optimisation abordée ici est du point de vue de la mémoire RAM. Nous cherchons à fusionner la table de routage avec la table \acrshort{DAD} (duplication d'adresse).

\begin{figure}[!h]
    \centering
    \includegraphics[width=15cm]{images/optimisation/fusion_table.png}
    \caption{Fusion de la table de duplication avec celle de routage}
    \label{fig:fusion_table}
\end{figure}

A l'origine, nous disposons de 3 tables (figure \ref{fig:fusion_table}):
\begin{itemize}
\item La table de routage (\acrshort{RT}): contenant des adresses globales \acrshort{IPv6} construites sur l'adresse link local sur 16 bits (\acrshort{GP16}), associées à une adresse locale \acrshort{IPv6}.
\item Le neigbor cache (\acrshort{NC}): contenant l'adresse locale \acrshort{IPv6} associée à la \acrshort{MAC} adresse.
\item La table de duplication d'adresse (\acrshort{DADT}): contenant l'association \acrshort{GP16} avec une adresse MAC unique
\end{itemize}

L'objectif de cette optimisation est de supprimer la redondance de données. Il existe une association entre une adresse \acrshort{GP16} et  l'adresse MAC via l'adresse \acrshort{IPv6} locale dissimulée dans la \acrshort{RT} et la \acrshort{NC}. Nous avons un couple similaire pour la table \acrshort{DAD}. Une adresse \acrshort{EUI-16} n'étant pas unique (voir section \ref{sec:eui_16_64}), nous sommes dans l'obligation d'utiliser EUI-64. Le dénominateur commun pour fusionner ne peut qu'être l'adresse \acrshort{GP16}. Nous décidons d'ajouter l'ensemble de la structure de la table \acrshort{DAD}, à exception du champ \acrshort{GP16}, dans la structure de la table de routage. Cette opération est similaire au choix fait par RPL dans Contiki.

Le \acrshort{6LBR} possède l'ensemble des adresses globales pour le routage en Router-Over et pour vérifier la duplication. Il y aura donc exactement le même nombre d'entrées dans la \acrshort{RT} que dans la \acrshort{DADT}. Le gain mémoire sera donc:
\begin{center}
$Memory = (num_{6LH}+num_{6LR})*128 [bits]$
\end{center}

Remarquons, si certains appareils configurent leurs adresses avec \acrshort{EUI-16} et d'autres avec \acrshort{EUI-64}, nous aurons de la résolution de duplication d'adresse multihop pour les \acrshort{GP16}, mais pas \acrshort{GP64}. Par conséquent, le nombre d'entrées dans \acrshort{DADT} sera plus petit que dans la \acrshort{RT}. Si la différence est extrêmement grande, l'optimisation s'inverse. En effet, il faut stocker des variables pour la \acrshort{DADT}, dont le lifetime, alors qu'elles sont inutiles. Cette amélioration prendra tout son sens si nous utilisons moins de $2^{15}$ capteurs et les configurerions avec des adresses basées sur l'\acrshort{EUI-16}.

Néanmoins, nous devons faire attention à ne pas supprimer des entrées alors qu'elles ne doivent pas l'être. En effet, \acrshort{RPL} et \acrshort{6LoWPAN-ND} travaillent sur une même ressource pouvant produire des confusions. Nous devons éviter cela et donc accroître l'implémentation, produisant un surcoût de la mémoire de code. Ce prix est fixe et ne varie pas en fonction du nombre d'entrées dans la table. Généralement, les 6LBR possèdent plus de ressources ce qui résout le problème.

La macro \emph{CONF\_6LOWPAN\_ND\_OPTI\_TDAD} est utilisée pour activer l'implémentation et n'est applicable qu'aux \acrshort{6LBR}.


\subsection{Fusion DAD - DAO}
Le projet dans cette section est de réduire le nombre de messages en unifiant le mécanisme \acrshort{DAD} de \acrshort{6LoWPAN-ND} (message \acrshort{DAR} et \acrshort{DAC}) avec les messages \acrshort{RPL} \acrshort{DAO} et \acrshort{DAO-ACK}. La motivation principale réside dans leurs mécanismes similaires et donc redondants.

La macro \emph{CONF\_6LOWPAN\_ND\_OPTI\_FUSION} est utilisée pour activer l'implémentation de l'optimisation dans les entités \acrshort{6R}.

\subsubsection{Redondance}
\begin{figure}[!htbp]
    \centering
    \subfloat[Sans optimisation]{
    \includegraphics[width=7cm]{images/optimisation/fusion_msg_without_optim.png}
    \label{fig:fusion_msg_without}
    }
    \hfill
    \subfloat[Avec optimisation]{
    \includegraphics[width=7cm]{images/optimisation/fusion_msg_with_optim.png}
    \label{fig:fusion_msg_with}
    }
    \caption{Remplacement des messages DAR et DAC par une option dans DAO et DAO-ACK}
    \label{fig:fusion_msg_with}
\end{figure}


Sans optimisation, \acrshort{6LoWPAN-ND} effectue un envoi successif de messages \acrshort{NS}, \acrshort{DAR}, \acrshort{DAC} et \acrshort{NA} (figure \ref{fig:fusion_msg_without}) pour la procédure DAD. Ensuite, \acrshort{RPL} envoie un \acrshort{DIO} du \acrshort{6LBR} vers les feuilles permettant l'envoi d'un \acrshort{DAO} afin d'ajouter les entrées dans chaque noeud pour former la route downward. Nous pouvons regrouper ce double échange, car il se déroule à peu d'intervalles. Sur la figure \ref{fig:fusion_msg_with}, nous observons qu'il n'y a plus que 4 messages au lieu de 6. Pour ce faire, nous utilisons des messages \acrshort{DAO} avec deux nouvelles options RPL, similaire aux messages de duplication adresse: \gls{DARO} et \gls{DACO}. Ils remplacent les \gls{DAM} et permettent de propager la nouvelle adresse dans chaque senseur vers le \acrshort{6LBR}. L'entrée ajoutée dans les \acrshort{RT} n'est toutefois que temporaire, car elle ne sera valide qu'à condition d'avoir un \acrshort{DAO-ACK} contenant un \acrshort{DACO} avec un état \emph{SUCCESS}. 

Nous aurions pu ajouter l'entrée uniquement à la réception du message \acrshort{DAO-ACK}. Cependant cela est impossible, car l'adresse de la \acrshort{RT} est délivrée par l'option RPL Target absente dans les messages  \acrshort{DAO-ACK} (section \ref{sec:rpl_options}).
L'interaction \acrshort{DIO} - \acrshort{DAO} existe toujours. Nous ne voulons pas en plus avoir un \acrshort{DAO-ACK} à chaque DAO envoyé. Pour ce faire, nous n'utilisons pas le flag K dans l'en-tête du message \acrshort{DAO} (\ref{fig:dao}), car nous transmettons un \acrshort{DAO-ACK} sur une "route" et pas à chaque message \acrshort{DAO}.  Si un \acrshort{6LR} reçoit un \acrshort{DAO} avec une option \acrshort{DARO}, alors, l'entrée dans la \acrshort{RT} est en attente d'un accusé de réception, aussi non, elle est ajoutée directement (état).

\subsubsection{Option}
En vue de la section précédente, nous avons besoin de spécifier deux nouvelles options \acrshort{RPL}, que nous appelons: \acrshort{DARO} pour \acrlong{DARO} (figure \ref{fig:header_daro}) et \acrshort{DACO} pour \acrlong{DACO} (figure \ref{fig:header_daco}). Pour ce faire, nous nous basons sur l'en-tête \acrlong{DAM} spécifiée dans le RFC6775 \cite{rfc6775} (voir figure \ref{fig:header_dam}). Ils doivent s’appuyer sur le format générique des options RPL (voir figure \ref{fig:rploptgen}). Des modifications doivent être apportées propre au passage d'un header \acrshort{ICMPv6} à une option \acrshort{RPL}:
\begin{itemize}
\item Le champ "Code" devient un champ "Length" représentant la longueur en octets de l'option; ici il vaut respectivement 12 et 28 pour \acrshort{DARO} et \acrshort{DACO}.
\item Le champ "Type" était de 157 pour \acrshort{DAR} et 158 pour \acrshort{DAC} et doit être défini par un nouveau standard. Pour notre implémentation, nous gardons les mêmes valeurs dans les options respectives.
\item Le Checksum n'a plus lieu d'être, car il est dissimulé dans le header des messages \acrshort{RPL}.
\end{itemize}

Le champ \emph{Registered Address} n'apparaît pas dans l'option \acrshort{DARO}, car cette information est déjà présente dans l'option \emph{RPL Target}. Le champ \emph{Lifetime} ne peut être fusionné avec l'option \emph{Transit Information}, car l'assignation se fait sur 8 bits au lieu de 16 bits.

\begin{figure}[!htbp]
\centering
\begin{small}
\begin{BVerbatim}
 0                   1                   2                   3
 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|     Type      |   Length=12   |    Status     |   Reserved    |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|     Registration Lifetime     |                               |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+                               +
|                            EUI-64                             |
+                               +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                               |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
\end{BVerbatim}
\end{small}
\caption{Duplication Address Request Option - DARO}
\label{fig:header_daro}
\end{figure}

\begin{figure}[!htbp]
\centering
\begin{small}
\begin{BVerbatim}
 0                   1                   2                   3
 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|     Type      |   Length=28   |    Status     |   Reserved    |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|     Registration Lifetime     |                               |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+                               +
|                             EUI-64                            |
+                               +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                               |                               |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+                               +
|                                                               |
+                                                               +
|                                                               |
+                       Registered Address                      +
|                                                               |
+                               +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                               |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
\end{BVerbatim}
\end{small}
\caption{Duplication Address Confirmation Option - DACO}
\label{fig:header_daco}
\end{figure}

\subsubsection{Conséquence}
Cette amélioration permet d'économiser une grande partie de l'énergie en envoyant 4 messages au lieu de 6.  Cette interaction est la plus utilisée dans \acrshort{6LoWPAN-ND} rendant le gain conséquent. Néamoins, il est difficile de quantifier de manière théorique le nombre total de messages envoyés. En effet, le management des timers entre \acrshort{RPL} et \acrshort{6LoWPAN-ND}  est différent. Le nouvel échange est similaire aux nombres de messages envoyés uniquement par \acrshort{6LoWPAN-ND} : 4. Le gain substantiel est donné par le nombre de messages que RPL n'envoie plus avec l'amélioration.

Le phénomène décrit dans la section \ref{sec:insistance_ns} est représenté par la figure \ref{fig:problem_dar} et sera atténué par cette optimisation. En construisant les tables de routage tout en validant l'adresse, cela nous permet de n'avoir aucun délai entre le protocole \acrshort{6LoWPAN-ND} et \acrshort{RPL}. Sur la figure \ref{fig:problem_dar}, cette propriété garantit que l'ensemble des routes est présent dans la \acrshort{RT} avant que le \acrshort{6LH} n'envoie un \acrshort{NS}. Mais, pour envoyer un \acrshort{DARO}, il nous faut un parent RPL, donc nous déplaçons le problème du \acrshort{6LBR} vers le \acrshort{6LR} initiateur du message.

Le chemin upward que fait le \acrshort{DARO} n'est plus déterminé par \acrshort{6LoWPAN-ND} mais par \acrshort{RPL}. En conséquence, nous serons susceptibles d'avoir des problèmes si un des deux protocoles n'est pas présent dans tous les \acrshort{6LBR} de manière synchrone.


\subsection{Conclusion}
Les améliorations formulées sont compatibles entre elles. Leurs utilisations dépendent de la demande de l'utilisateur et de son \acrlong{WSN}. Les deux premières offrent d'assez bons avantages et peu d'influence sur les spécifications RFC. La troisième rend le code source moins facile à la lecture et l’efficacité dépendra de la topologie employée. La dernière est plus nuancée, car nous n'avons plus la séparation entre les deux protocoles. Elle permet d’économiser l'envoi de message, mais peut engendrer des incompatibilités de senseurs avec et sans cette optimisation.



