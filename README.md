#Master's thesis
Téléchargement de la version pdf [ici](https://bytebucket.org/sdefauw/memoire/raw/bbcea8351661e62f5aecf97bc66a58536f79fc39/me%CC%81moire.pdf)

#Source code
Code source [github](https://github.com/sdefauw/contiki/)

#Presentation
Présentation disponible en [pptx](https://bitbucket.org/sdefauw/memoire/src/c2f986f11e0b3c8f4620291e8e8904a221e24208/presentation/presentation.pptx?at=master) ou en [pdf](https://bytebucket.org/sdefauw/memoire/raw/c2f986f11e0b3c8f4620291e8e8904a221e24208/presentation/presentation.pdf)