MAINDOC = document


all: clean pdf bib glo 2pdf

short: pdf


pdf:
	pdflatex $(MAINDOC).tex

2pdf:
	pdflatex $(MAINDOC).tex; \
	pdflatex $(MAINDOC).tex;

bib:
	bibtex $(MAINDOC)

glo:
	makeglossaries $(MAINDOC)


clean:
	rm -rf *.acn *.acr *.alg *.aux *.bbl *.blg *.cb* *.dvi *.lof *.log *.lot *.maf *.mtc *.out *.toc *.glg *.glo *.gls *.ist
